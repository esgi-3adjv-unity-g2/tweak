﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PhysicInteruptor : MonoBehaviour {
    [SerializeField]
    float massToActive;

    public bool isOk = false;

    private bool isActivate = false;

    private List<Rigidbody> rigidBodys = new List<Rigidbody>();
    private float totalMass = 0;

    public UnityEvent onActivate = new UnityEvent();
    public UnityEvent onDesactivate = new UnityEvent();

    void Update()
    {
        totalMass = 0;
        foreach(Rigidbody rb in rigidBodys)
        {
            totalMass += rb.mass;
        }

        if(totalMass >= massToActive)
        {
            isOk = true;
        }
        else
        {
            isOk = false;
        }

        if(isOk && !isActivate)
        {
            isActivate = true;
            onActivate.Invoke();
        }
        else if(!isOk && isActivate)
        {
            isActivate = false;
            onDesactivate.Invoke();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(!rigidBodys.Contains(other.GetComponent<Rigidbody>()))
        {
            rigidBodys.Add(other.GetComponent<Rigidbody>());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(rigidBodys.Contains(other.GetComponent<Rigidbody>()))
        {
            rigidBodys.Remove(other.GetComponent<Rigidbody>());
        }
    }
}
