﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIAmmo : MonoBehaviour {

    [SerializeField]
    RectTransform maskTransform;

    [SerializeField]
    RectTransform ammoTransform;

    [SerializeField]
    PlayerPlatform.PlatformType ammoType;

    [SerializeField]
    Image ammoImage;

    [SerializeField]
    Sprite ammoInUseImage;

    PlayerPlatform pInstance;

    Sprite defaultAmmoImage;

    void Start()
    {
        defaultAmmoImage = ammoImage.sprite;
    }
	
	void Update () {
		if (pInstance == null)
        {
            pInstance = PlayerPlatform.Instance;
            return;
        }

        if (ammoType == PlayerPlatform.PlatformType.BLUE)
        {
            float maskLeftMargin = pInstance.bluePlatformGauge / pInstance.blueMatterLimit;
            maskTransform.anchorMax = new Vector2(maskLeftMargin, 1f);

            if (maskLeftMargin != 0f)
                ammoTransform.anchorMax = new Vector2(1f / maskLeftMargin, 1f);
            else
                ammoTransform.anchorMax = new Vector2(1f, 1f);
        }
        else if (ammoType == PlayerPlatform.PlatformType.RED)
        {
            float maskLeftMargin = pInstance.redPlatformGauge / pInstance.redMatterLimit;
            maskTransform.anchorMax = new Vector2(maskLeftMargin, 1f);

            if (maskLeftMargin != 0f)
                ammoTransform.anchorMax = new Vector2(1f / maskLeftMargin, 1f);
            else
                ammoTransform.anchorMax = new Vector2(1f, 1f);
        }

        if (pInstance.currentType == ammoType)
        {
            if (ammoImage.sprite != ammoInUseImage)
                ammoImage.sprite = ammoInUseImage;
        }
        else
        {
            if (ammoImage.sprite == ammoInUseImage)
                ammoImage.sprite = defaultAmmoImage;
        }
	}
}
