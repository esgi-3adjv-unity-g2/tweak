﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectUIAction : MonoBehaviour {

    [SerializeField]
    HomeGUIScript homeUIController;

    [SerializeField]
    CanvasGroup group;

    [SerializeField]
    GameObject levelClearedText;

    [SerializeField]
    Text timePassed;

    [SerializeField]
    Text dieCounter;

    int selectedSceneIndex;

    public void SetSelectedScene(int levelId)
    {
        selectedSceneIndex = levelId;
    }

    public void RefreshData(int levelId)
    {
        SetSelectedScene(levelId);

        var playerProgress = GameSaveManager.Instance.GetLevelState(selectedSceneIndex);

        group.interactable = true;
        group.alpha = 1f;

        if (playerProgress == null)
        {
            return;
        }

        if (playerProgress.cleared)
            levelClearedText.SetActive(true);
        else
            levelClearedText.SetActive(false);

        dieCounter.text = playerProgress.dieCounter.ToString();
        timePassed.text = string.Format("{0:00}:{1:00}", Mathf.Floor(playerProgress.timer / 60), Mathf.RoundToInt(playerProgress.timer % 60));
    }

    public void LoadLevel()
    {
        homeUIController.LoadLevel(selectedSceneIndex);
    }

}
