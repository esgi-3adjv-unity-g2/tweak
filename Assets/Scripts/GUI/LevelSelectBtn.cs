﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LevelSelectBtn : MonoBehaviour {

    [SerializeField]
    int levelId;

	void Start ()
    {
        var playerProgress = GameSaveManager.Instance.GetLevelState(levelId);

        if (playerProgress.previousLevel != -1)
        {
            var previousLevel = GameSaveManager.Instance.GetLevelState(playerProgress.previousLevel);
            if ( ! previousLevel.cleared)
            {
                GetComponent<Button>().interactable = false;
            }
        }
	}

}
