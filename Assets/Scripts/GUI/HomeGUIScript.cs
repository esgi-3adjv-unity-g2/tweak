﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeGUIScript : MonoBehaviour {

    [SerializeField]
    Animator homeUIAnimator;

    [SerializeField]
    Animator cameraAnimator;

    [SerializeField]
    CanvasGroup homeUIGroup;

    [SerializeField]
    CanvasGroup levelUIGroup;

    void Start()
    {
        homeUIGroup.interactable = true;
    }

    public void ShowLevelUI()
    {
        homeUIAnimator.SetTrigger("ShowLevel");
        cameraAnimator.SetBool("Home", false);

        homeUIGroup.interactable = false;
        levelUIGroup.interactable = true;
    }

    public void ShowHomeUI()
    {
        homeUIAnimator.SetTrigger("ShowHome");
        cameraAnimator.SetBool("Home", true);

        homeUIGroup.interactable = true;
        levelUIGroup.interactable = false;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadLevel(int levelId)
    {
        SceneManager.LoadScene(levelId);
    }

}
