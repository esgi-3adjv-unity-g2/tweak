﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGUIManager : MonoBehaviour {

    [SerializeField]
    CanvasGroup group;

    [SerializeField]
    FollowCamera cameraManager;
	
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            group.alpha = 1f;
            group.interactable = true;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            cameraManager.freeze = true;
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ResumeGame()
    {
        group.alpha = 0f;
        group.interactable = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cameraManager.freeze = false;
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

}
