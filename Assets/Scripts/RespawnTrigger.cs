﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RespawnTrigger : MonoBehaviour {

    [SerializeField]
    Transform levelSpawn;

	void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovements>())
        {
            Rigidbody otherPhysic = other.GetComponent<Rigidbody>();
            otherPhysic.velocity = Vector3.zero;
            otherPhysic.MovePosition(levelSpawn.position);
            other.transform.rotation = levelSpawn.rotation;

            var level = GameSaveManager.Instance.GetLevelState(SceneManager.GetActiveScene().buildIndex);
            if (level != null)
            {
                level.DieOnLevel();
            }

            // CheckPoint.GoToCheckPoint();
        }
    }
}
