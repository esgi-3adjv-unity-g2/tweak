﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlatform : InteractivePlatform {

    [SerializeField]
    float meltSpeed;
	
	void FixedUpdate()
    {
        if (transform.localScale.z > 0.2f)
        {
            transform.localScale -= new Vector3(meltSpeed / 2, 0f, meltSpeed) * Time.fixedDeltaTime;
            transform.localPosition -= transform.forward * meltSpeed * Time.fixedDeltaTime * 0.5f;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

}
