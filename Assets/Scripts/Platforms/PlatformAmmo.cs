﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformAmmo : MonoBehaviour {

    [SerializeField]
    float matterAmout = 0f;

    [SerializeField]
    PlayerPlatform.PlatformType ammoType;
	
	void Update () {
        transform.Rotate(new Vector3(0f, 40f * Time.deltaTime, 20f * Time.deltaTime));
	}

    void OnTriggerEnter(Collider other)
    {
        PlayerPlatform platformManager = other.GetComponent<PlayerPlatform>();

        if (platformManager)
        {
            platformManager.AddMatter(matterAmout, ammoType);
            this.gameObject.SetActive(false);
        }
    }
}
