﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GrabbableObject : MonoBehaviour {

    Rigidbody physic;

    void Start()
    {
        physic = GetComponent<Rigidbody>();
    }

    public Rigidbody GetRigidbody()
    {
        return physic;   
    }

}
