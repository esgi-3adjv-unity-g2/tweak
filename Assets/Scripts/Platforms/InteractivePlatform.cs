﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractivePlatform : MonoBehaviour {

    [SerializeField]
    PlayerPlatform.PlatformType plateformeType;

    public PlayerPlatform.PlatformType PlateformType { get; }
    
}
