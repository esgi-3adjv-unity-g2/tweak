﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    [SerializeField]
    Vector3 destination;

    [SerializeField]
    float speed;

    [SerializeField]
    Transform childPlatform;

    [SerializeField]
    BoxCollider platformTrigger;

    Vector3 initialPosition;

    Vector3 targetPosition;

	void Start () {
        initialPosition = transform.position;
        targetPosition = transform.position + destination;

        platformTrigger.size = new Vector3(childPlatform.transform.localScale.x, 0.2f, childPlatform.transform.localScale.z);
        platformTrigger.center = new Vector3(0f, childPlatform.transform.localScale.y / 2f + 0.1f, 0f);
    }
	
	void FixedUpdate()
    {
        float distanceFromDestination = Vector3.SqrMagnitude(targetPosition - transform.position);

        if (distanceFromDestination < 0.01f)
        {
            var tmp = initialPosition;
            initialPosition = targetPosition;
            targetPosition = tmp;
        }
        transform.position = transform.position + Vector3.Normalize(targetPosition - transform.position) * speed * Time.fixedDeltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.parent == null)
            other.gameObject.transform.SetParent(transform);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.transform.parent == transform)
            other.gameObject.transform.parent = null;
    }

}
