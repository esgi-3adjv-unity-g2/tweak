﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{

    public static Vector3 position;
    public static Quaternion rotation;

    public static PlayerMovements player;

    // Use this for initialization
    void Start()
    {
        if(player == null)
        {
            player = FindObjectOfType<PlayerMovements>();
            position = player.transform.position;
            rotation = player.transform.rotation;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player.gameObject)
        {
            position = other.transform.position;
            rotation = other.transform.rotation;
        }

        gameObject.SetActive(false);
    }

    public static void GoToCheckPoint()
    {
        Debug.Log(position);
        player.transform.position = position;
        player.transform.rotation = rotation;
    }
}
