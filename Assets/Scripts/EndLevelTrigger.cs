﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndLevelTrigger : MonoBehaviour {

    [SerializeField]
    GameController controller;

	void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovements>())
        {
            controller.EndLevel();

            var level = GameSaveManager.Instance.GetLevelState(SceneManager.GetActiveScene().buildIndex);
            if (level != null && level.nextLevel != -1)
            {
                SceneManager.LoadScene(level.nextLevel);
            }
        }
    }
}
