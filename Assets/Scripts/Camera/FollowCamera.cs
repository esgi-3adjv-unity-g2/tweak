﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FollowCamera : MonoBehaviour {

    Transform cameraTarget;

    [SerializeField]
    Transform leftTarget;

    [SerializeField]
    Transform rightTarget;

    [SerializeField]
    float friction = 1;

    [Range(0.5f, 30f)]
    [SerializeField]
    float distanceOfTarget;

    float pitchZ = 20f;

    float verticalAxis;

    public bool freeze = false;

    void Start()
    {
        cameraTarget = leftTarget;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        verticalAxis = Input.GetAxis("Pitch Y");

        if (Input.GetButtonDown("Switch Camera Side"))
        {
            if (cameraTarget == leftTarget)
                cameraTarget = rightTarget;
            else
                cameraTarget = leftTarget;
        }
    }

    void FixedUpdate()
    {
        if ( !freeze)
        {
            float pitchY = Mathf.Deg2Rad * Mathf.LerpAngle(transform.eulerAngles.y, cameraTarget.eulerAngles.y, Time.deltaTime * friction);
            pitchZ = Mathf.Clamp(pitchZ + verticalAxis * Time.deltaTime, -85f, 85f);

            float r = distanceOfTarget * Mathf.Cos(0) * Mathf.Cos(Mathf.Deg2Rad * -pitchZ);

            Vector3 cameraPosition = new Vector3(
                r * Mathf.Sin(pitchY),
                distanceOfTarget * Mathf.Cos(0) * Mathf.Sin(Mathf.Deg2Rad * -pitchZ),
                r * Mathf.Cos(pitchY) * Mathf.Cos(0)
            );
        
            transform.position = cameraTarget.position - cameraPosition;
            transform.LookAt(cameraTarget);
        }
    }

}
