﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[System.Serializable]
public class LevelState
{

    public bool cleared = false;

    public float timer = 0.0f;

    public int dieCounter = 0;

    public int previousLevel;

    public int nextLevel;

    public LevelState(int prev, int next)
    {
        nextLevel = next;
        previousLevel = prev;
    }

    public void SetClear(float t)
    {
        timer = t;
    }

    public void DieOnLevel()
    {
        dieCounter++;
    }

}

public class GameSaveManager : MonoBehaviour {

    #region UnityCompliant Singleton
    public static GameSaveManager Instance { get; private set; }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;

            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    [SerializeField]
    Dictionary<int, LevelState> levels;

    [SerializeField]
    string savePath;

    void Start()
    {
        LoadSavedGame();
    }

    public void LoadSavedGame()
    {
        BinaryFormatter format = new BinaryFormatter();

        try
        {
            FileStream saveFile = File.Open(Application.persistentDataPath + "/saves/" + savePath, FileMode.Open);
            levels = format.Deserialize(saveFile) as Dictionary<int, LevelState>;
            saveFile.Close();
        }
        catch (SerializationException e)
        {
            Debug.LogError(e.Message);
            InitLevelData();
            return;
        }
        catch (IOException e)
        {
            Debug.LogError(e.Message);
            InitLevelData();
            return;
        }
        catch (System.IO.IsolatedStorage.IsolatedStorageException e)
        {
            //Debug.LogError(e.Message);
            InitLevelData();
            return;
        }
    }

    public void SaveGame()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/saves"))
        {
            try
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/saves");
            }
            catch (IOException e)
            {
                //Debug.LogError(e.Message);
            }
        }

        try
        {
            BinaryFormatter format = new BinaryFormatter();
            FileStream saveFile = File.Create(Application.persistentDataPath + "/saves/" + savePath);
            Debug.Log(Application.persistentDataPath + "/saves/" + savePath);
            format.Serialize(saveFile, levels);
            saveFile.Close();
        }
        catch (IOException e)
        {
            //Debug.LogError(e.Message);
        }
    }

    void InitLevelData()
    {
        levels = new Dictionary<int, LevelState>();

        LevelState l = new LevelState(-1, 2);
        levels.Add(1, l);
        l = new LevelState(1, 3);
        levels.Add(2, l);
        l = new LevelState(2, 4);
        levels.Add(3, l);
        l = new LevelState(3, 5);
        levels.Add(4, l);
        l = new LevelState(4, 6);
        levels.Add(5, l);
        l = new LevelState(5, 7);
        levels.Add(6, l);
        l = new LevelState(6, 0);
        levels.Add(7, l);

        SaveGame();
    }

    public LevelState GetLevelState(int id)
    {
        if (levels == null)
        {
            LoadSavedGame();
        }

        if (levels.ContainsKey(id))
        {
            return levels[id];
        }
        else
        {
            return null;
        }
    }

}
