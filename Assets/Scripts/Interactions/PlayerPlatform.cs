﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerPlatform : MonoBehaviour {

    #region UnityCompliant Singleton
    public static PlayerPlatform Instance { get; private set; }

    public virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            return;
        }

        Destroy(this.gameObject);
    }
    #endregion

    public enum PlatformType { BLUE, RED, GRAB };

    #region BluePlatform
    [Header("Blue Platform")]
    public float bluePlatformGauge = 0f;

    public float blueMatterLimit = 100f;

    [SerializeField]
    float bluePlatformMaxSize = 5f;

    [SerializeField]
    float bluePlatformSpeed = 0.5f;

    [SerializeField]
    Material bluePlatformDefaultMaterial;

    [SerializeField]
    Material bluePlatformActiveMaterial;

    [SerializeField]
    GameObject bluePlatformPrefab;

    [SerializeField]
    Color blueCrosshairColor;
    #endregion

    #region RedPlatform
    [Header("Red Platform")]
    public float redPlatformGauge = 0f;

    public float redMatterLimit = 200f;

    [SerializeField]
    float redPlatformMaxSize = 3f;

    [SerializeField]
    float redPlatformLoadingRate = 1f;

    [SerializeField]
    GameObject redLoadingEffect;

    [SerializeField]
    GameObject redPlatformPrefab;

    [SerializeField]
    Material redPlatformDefaultMaterial;

    [SerializeField]
    Material redPlatformActiveMaterial;

    [SerializeField]
    Color redCrosshairColor;
    #endregion

    #region Grabber
    [Header("Grabber")]
    [SerializeField]
    SpringJoint grabberJoin;

    [SerializeField]
    float grabForce;

    [SerializeField]
    LineRenderer grabRender;

    [SerializeField]
    Color grabberCrosshairColor;

    GrabbableObject objectGrabbed;

    bool inGrab = false;
    #endregion

    #region Materials
    [Space]
    [Header("Player's materials")]
    [SerializeField]
    SkinnedMeshRenderer skin;
    [SerializeField]
    Material skinBluePower;
    [SerializeField]
    Material skinRedPower;
    [SerializeField]
    Material skinBase;
    #endregion

    [Space(10)]
    [Header("Spawner settings")]
    public PlatformType currentType;

    [SerializeField]
    Collider bottomCollider;

    [SerializeField]
    Image crosshairImage;

    MeshRenderer render;

    bool fireKeyDown = false;
    bool inhaleKey = false;

    GameObject bluePlatform;
    GameObject invertBluePlatform;

    float redPlatformLoad;
    bool redInLoad = false;

    Camera mainCamera;
    
	void Start () {
        mainCamera = Camera.main;
        render = GetComponent<MeshRenderer>();
        render.material = bluePlatformDefaultMaterial;
    }
	
	void Update () {
        // Select current mode
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentType = PlatformType.BLUE;
            render.material = bluePlatformDefaultMaterial;
            crosshairImage.color = blueCrosshairColor;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentType = PlatformType.RED;
            render.material = redPlatformDefaultMaterial;
            crosshairImage.color = redCrosshairColor;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentType = PlatformType.GRAB;
            crosshairImage.color = grabberCrosshairColor;
        }

        if (!fireKeyDown)
            fireKeyDown = Input.GetButtonDown("Fire");

        inhaleKey = Input.GetButton("Inhale");

        if (fireKeyDown)
        {
            fireKeyDown = false;

            if (currentType == PlatformType.BLUE && bluePlatformGauge > 3f)
            {
                bluePlatformGauge -= 3f;
                bluePlatform = Instantiate(bluePlatformPrefab) as GameObject;
                bluePlatform.transform.position = transform.position + 
                    (transform.forward * 2.5f) - new Vector3(0f, transform.lossyScale.y / 2 + bluePlatform.transform.lossyScale.y, 0f);

                MeshRenderer blueMesh = bluePlatform.GetComponent<MeshRenderer>();
                blueMesh.material = bluePlatformActiveMaterial;
                render.material = bluePlatformActiveMaterial;
            }
            else if (currentType == PlatformType.RED && redPlatformGauge > 0.8f)
            {
                redPlatformGauge -= 0.8f;
                redPlatformLoad = 0.8f;
                render.material = redPlatformActiveMaterial;
                redLoadingEffect.SetActive(true);
                redInLoad = true;
            }
            else if (currentType == PlatformType.GRAB)
            {
                RaycastHit hit;
                if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, 30f))
                {
                    objectGrabbed = hit.collider.gameObject.GetComponent<GrabbableObject>();
                    GrabObject(objectGrabbed, hit.point);
                }
            }
        }
        else
        {
            skin.material = skinBase;
        }

        if (Input.GetButton("Fire"))
        {
            if (currentType == PlatformType.BLUE && bluePlatform && bluePlatformGauge > 0f && bluePlatform.transform.lossyScale.x < bluePlatformMaxSize)
            {
                skin.material = skinBluePower;
                bluePlatform.transform.localScale += new Vector3(bluePlatformSpeed, 0f, bluePlatformSpeed) * Time.deltaTime;
                bluePlatform.transform.position += bluePlatform.transform.forward * bluePlatformSpeed / 2f * Time.deltaTime;
                bluePlatformGauge -= 1f * Time.deltaTime;
            }
            else if (redInLoad)
            {
                float incLoad = redPlatformLoad + redPlatformLoadingRate * Time.deltaTime;
                if (incLoad < redPlatformMaxSize)
                {
                    skin.material = skinRedPower;
                    redPlatformLoad = incLoad;
                    redPlatformGauge -= redPlatformLoadingRate * Time.deltaTime;
                }
                else
                {
                    redLoadingEffect.SetActive(false);
                }
            }
        }

        if (Input.GetButtonUp("Fire"))
        {
            if (bluePlatform)
            {
                MeshRenderer blueMesh = bluePlatform.GetComponent<MeshRenderer>();
                blueMesh.material = bluePlatformDefaultMaterial;
                render.material = bluePlatformDefaultMaterial;
                bluePlatform = null;
            }
            else if (redInLoad)
            {
                RaycastHit hit;
                if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, 30f, 1 << 0))
                {
                    GameObject redPlatform = Instantiate(redPlatformPrefab, hit.point, Quaternion.LookRotation(hit.normal)) as GameObject;
                    redPlatform.transform.localScale = new Vector3(2f, 0.3f, redPlatformLoad);
                    redPlatform.transform.localPosition += redPlatform.transform.forward * redPlatform.transform.localScale.z / 2;
                }
                else
                {
                    redPlatformGauge += redPlatformLoad;
                }

                render.material = redPlatformDefaultMaterial;
                redLoadingEffect.SetActive(false);
                redInLoad = false;
            }

            if (inGrab)
            {
                UnGrabObject();
            }
        }
	}

    void FixedUpdate()
    {
        if (inhaleKey && ! invertBluePlatform)
        {
            float bottomBound = bottomCollider.bounds.extents.y;

            RaycastHit hit;
            if (Physics.Raycast(transform.position, new Vector3(0f, -1f, 0f), out hit, bottomBound + 0.3f))
            {
                BluePlatform platform = hit.transform.gameObject.GetComponent<BluePlatform>();
                if (platform)
                {
                    invertBluePlatform = platform.gameObject;
                    MeshRenderer blueMesh = invertBluePlatform.GetComponent<MeshRenderer>();
                    blueMesh.material = bluePlatformActiveMaterial;
                    render.material = bluePlatformActiveMaterial;
                }
            }
        }

        if (inhaleKey && invertBluePlatform && invertBluePlatform.transform.lossyScale.x > 1f)
        {
            invertBluePlatform.transform.localScale -= new Vector3(bluePlatformSpeed, 0f, bluePlatformSpeed) * Time.deltaTime;
            invertBluePlatform.transform.position -= invertBluePlatform.transform.forward * bluePlatformSpeed / 2f * Time.deltaTime;
            bluePlatformGauge += (bluePlatformGauge + 1f * Time.deltaTime < blueMatterLimit) ? 1f * Time.deltaTime : blueMatterLimit;
        }

        if ( ! inhaleKey && invertBluePlatform)
        {
            MeshRenderer blueMesh = invertBluePlatform.GetComponent<MeshRenderer>();
            blueMesh.material = bluePlatformDefaultMaterial;
            render.material = bluePlatformDefaultMaterial;
            invertBluePlatform = null;
        }

        if (currentType == PlatformType.GRAB && inGrab)
        {
            if (inhaleKey)
            {
                grabberJoin.spring = grabForce;
            }
            else
            {
                grabberJoin.spring = 10.0f;
            }
        }

        if (inGrab)
        {
            grabRender.SetPosition(0, transform.position);
            grabRender.SetPosition(1, grabberJoin.connectedAnchor);
        }
        
    }

    void GrabObject(GrabbableObject objectToGrab, Vector3 anchorPoint)
    {
        if (objectToGrab != null)
        {
            grabberJoin.autoConfigureConnectedAnchor = true;
            grabberJoin.connectedBody = objectToGrab.GetRigidbody();
        }
        else
        {
            grabberJoin.autoConfigureConnectedAnchor = false;
            grabberJoin.connectedAnchor = anchorPoint;
        }
        
        grabRender.enabled = true;
        inGrab = true;
    }

    void UnGrabObject()
    {
        inGrab = false;
        grabRender.enabled = false;
        grabberJoin.connectedBody = null;
        grabberJoin.spring = 0.0f;
        objectGrabbed = null;
    }

    public void AddMatter(float amout, PlatformType type)
    {
        if (type == PlatformType.BLUE)
            bluePlatformGauge += (bluePlatformGauge + amout < blueMatterLimit) ? amout : blueMatterLimit;
        else if (type == PlatformType.RED)
            redPlatformGauge += (redPlatformGauge + amout < redMatterLimit) ? amout : redMatterLimit;
    }

}
