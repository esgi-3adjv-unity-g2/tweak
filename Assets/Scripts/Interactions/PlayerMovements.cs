﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovements : MonoBehaviour {

    [SerializeField]
    float playerForwardSpeed;

    [SerializeField]
    float playerStrafeSpeed;

    [SerializeField]
    float jumpForce;

    [Range(0, 4)]
    [SerializeField]
    int jumpLimit;

    [SerializeField]
    Collider bottomCollider;

    [SerializeField]
    float maxStrength;

    [SerializeField]
    Animator animator;

    Rigidbody physic;

    float horizontalAxis;
    float verticalAxis;
    float strafeAxis;
    float jumpAxis;

    public bool jumpKeyDown;
    public int jumpCount = 0;

    public List<GameObject> collisionWith;

	void Start () {
        physic = GetComponent<Rigidbody>();
        collisionWith = new List<GameObject>();
	}
	
	void Update () {
        horizontalAxis = Input.GetAxis("Pitch X");
        verticalAxis = Input.GetAxis("Forward");
        strafeAxis = Input.GetAxis("Strafe");
        jumpAxis = Input.GetAxis("Jump");

        if(verticalAxis != 0)
        {
            animator.enabled = true;
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
        if(!IsGrounded())
        {
            animator.enabled = false;
        }


        if (!jumpKeyDown)
            jumpKeyDown = Input.GetButtonDown("Jump");
	}

    void FixedUpdate()
    {
        bool isGrounded = IsGrounded();

        // Rotate player
        physic.angularVelocity = new Vector3(0f, horizontalAxis * Time.fixedDeltaTime);

        // Move player
        if (physic.velocity.x + physic.velocity.y < 0.2f)
            physic.velocity = new Vector3(0f, physic.velocity.y, 0f);
        
        Vector3 relativeForward = transform.TransformDirection(
            new Vector3(
                Mathf.Clamp(strafeAxis * playerStrafeSpeed * Time.fixedDeltaTime, -maxStrength, maxStrength),
                0f,
                Mathf.Clamp(verticalAxis * playerForwardSpeed * Time.fixedDeltaTime, -maxStrength, maxStrength)
            )
        );

        if (Mathf.Pow(relativeForward.x + relativeForward.y + relativeForward.z, 2) > 0.01f)
        {
            physic.AddForce(new Vector3(relativeForward.x, physic.velocity.y, relativeForward.z) - physic.velocity, ForceMode.VelocityChange);
        }
        
        // Jump
        if (isGrounded)
            jumpCount = 0;

        if (jumpKeyDown && (isGrounded || jumpCount < jumpLimit))
        {
            physic.AddForce(new Vector3(physic.velocity.x, jumpForce * jumpAxis, physic.velocity.z) - physic.velocity, ForceMode.VelocityChange);
            jumpCount++;
        }

        jumpKeyDown = false;
    }

    public bool IsGrounded()
    {
        float bottomBound = bottomCollider.bounds.extents.y;
        return Physics.Raycast(transform.position, new Vector3(0f, -1f, 0f), bottomBound + 0.1f);
    }

    void OnCollisionEnter(Collision other)
    {
        collisionWith.Add(other.gameObject);
    }

    void OnCollisionExit(Collision other)
    {
        collisionWith.Remove(other.gameObject);
    }

}
