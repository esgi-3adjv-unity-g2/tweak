﻿using UnityEngine;
using System.Collections;

public class GrabObject : MonoBehaviour {

    GameObject m_objectInUse;

    Rigidbody m_objectInUseRigidbody;

    bool m_grabKeyPressed;

    [SerializeField]
    KeyCode m_keyToGrab;

    [SerializeField]
    float m_playerGrabForce;

	void Update () {
        m_grabKeyPressed = (Input.GetKeyDown(m_keyToGrab));

        if (m_objectInUse)
        {
            var cameraLook = transform.forward;
            var objectPosition = transform.position + (cameraLook * 2.0f);
          
            if (Vector3.Distance(m_objectInUse.transform.position, objectPosition) < 0.1f)
            {
                m_objectInUseRigidbody.velocity = Vector3.zero;
            }
            else
            {
                float grabForce = (m_playerGrabForce - m_objectInUseRigidbody.mass < 0f) ? 0f : m_playerGrabForce - m_objectInUseRigidbody.mass;
                m_objectInUseRigidbody.velocity = (m_objectInUse.transform.position - objectPosition) * grabForce * -1f;
            }

            m_objectInUseRigidbody.MoveRotation(Quaternion.identity);
        }
	}

    void FixedUpdate()
    {
        if (m_grabKeyPressed)
        {
            if (m_objectInUse)
            {
                var meshRendered = m_objectInUse.GetComponent<MeshRenderer>();
                m_objectInUseRigidbody.freezeRotation = false;
                m_objectInUseRigidbody.useGravity = true;

                m_objectInUse = null;
                m_objectInUseRigidbody = null;
            }
            else
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 6f))
                {
                    var hitObject = hit.transform.gameObject;

                    var objectRigid = hitObject.GetComponent<Rigidbody>();
                    if (objectRigid != null)
                    {
                        m_objectInUse = hitObject;
                        m_objectInUseRigidbody = objectRigid;
                        m_objectInUseRigidbody.useGravity = false;
                        m_objectInUseRigidbody.freezeRotation = true;
                    }
                }
            }
        }
    }
}
