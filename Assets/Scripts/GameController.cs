﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    [SerializeField]
    string levelName;

    float startTime;

	void Start ()
    {
        startTime = Time.time;
	}

    public void EndLevel()
    {
        var level = GameSaveManager.Instance.GetLevelState(SceneManager.GetActiveScene().buildIndex);

        if (level != null)
        {
            float timeForClear = Time.time - startTime;

            if (timeForClear < level.timer || level.cleared == false)
            {
                level.timer = timeForClear;
            }

            level.cleared = true;
        }

        GameSaveManager.Instance.SaveGame();
    }

}
